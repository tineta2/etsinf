----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/28/2019 10:59:43 PM
-- Design Name: 
-- Module Name: TopLevel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TopLevel is
    Port ( tlrst_i : in STD_LOGIC;
           tlclk_i : in STD_LOGIC;
           en_i : in STD_LOGIC_VECTOR (1 downto 0);
           output_o : out STD_LOGIC_VECTOR (1 downto 0);
           tldata_o : out STD_LOGIC_VECTOR(3 downto 0));
end TopLevel;

architecture Behavioral of TopLevel is


component ROM is
    Port ( a_i : in STD_LOGIC_VECTOR (4 downto 0);
           d_o : out STD_LOGIC_VECTOR (3 downto 0));
end component;

component UpCounter is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           d_i : in STD_LOGIC_VECTOR(2 downto 0);
           q_o : out STD_LOGIC_VECTOR (2 downto 0);
           l_i : in STD_LOGIC);
end component;

--component sevensegment is
--    Port ( datos : in STD_LOGIC_VECTOR (2 downto 0);
          
--           salida : out STD_LOGIC_VECTOR (7 downto 0));
--end component;

signal S_rom, S_count : std_logic_vector(3 downto 0);

signal Q : std_logic_vector(2 downto 0);

signal A : std_logic_vector(4 downto 0);

signal data_aux : std_logic_vector(2 downto 0);


begin 

memrom : ROM port map (
            a_i => A,
            d_o => S_rom
);

upCont : UpCounter port map (
            clk_i => tlclk_i,
            rst_i => tlrst_i,
            d_i => S_count(2 downto 0),
            q_o => Q,
            l_i => S_count(3)
);

--sss : sevensegment port map (
--    datos => data_aux,
--    salida => tldata_o
--);

S_count <= S_rom;

A(0) <= Q(0);
A(1) <= Q(1);
A(2) <= Q(2);
A(3) <= en_i(0);
A(4) <= en_i(1);

output_o(0) <= Q(2);
output_o(1) <= Q(0);
tldata_o <= S_rom;


end Behavioral;
