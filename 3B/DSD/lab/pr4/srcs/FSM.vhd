----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.04.2019 15:50:52
-- Design Name: 
-- Module Name: FSM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           is_i : in STD_LOGIC_VECTOR (1 downto 0);
           b12_s : out STD_LOGIC_VECTOR (1 downto 0));
end FSM;

architecture Behavioral of FSM is

subtype state is STD_LOGIC_VECTOR(2 downto 0);

constant q0: state := "101";
constant q1: state := "111";
constant q2: state := "001";
constant q3: state := "100";
constant q4: state := "000";
constant q5: state := "010";

signal current_state : state := q0;

begin

transitions: process(rst_i, clk_i)
begin
    if rst_i = '1'then
        current_state <= q0;
    elsif clk_i'event and clk_i = '1' then
        case current_state is
            when q0 => 
                if is_i = "10" then
                    current_state <= q2;
                elsif is_i = "11" then
                    current_state <= q4;
                else 
                    current_state <= q0;
                end if;
            when q1 =>
                if is_i = "10" then
                    current_state <= q3;
                elsif is_i = "11" then
                    current_state <= q5;
                else current_state <= q1;
                end if;
            when q2 =>
                if is_i = "00" then
                    current_state <= q1;
                elsif is_i = "11" then
                    current_state <= q5;
                else current_state <= q2;
                end if;
            when q3 =>
                if is_i = "00" then
                    current_state <= q0;
                elsif is_i = "11" then
                    current_state <= q4;
                else current_state <= q3;
                end if;
             when q4 =>
                if is_i = "00" then
                    current_state <= q0;
                elsif is_i = "10" then
                    current_state <= q2;
                else current_state <= q4;
                end if;        
             when q5 => 
                if is_i = "00" then
                    current_state <= q1;
                elsif is_i = "10" then
                    current_state <= q3;
                else current_state <= q5;
                end if;
             when others =>
                current_state <= q0;
        end case;
    end if;
end process;

outputs: process(current_state)
begin
    case current_state is
        when q0 => 
          b12_s <= "11";
        
        when q1 => 
          b12_s <= "11";
          
          when q2 => 
           b12_s <= "10";
                  
       when q3 => 
         b12_s <= "01";
                            
        
      when q4 => 
       b12_s <= "00";
                   
     when q5 => 
       b12_s <= "00";
                             
     when others =>
        b12_s <= "00";
    end case;
end process;

end Behavioral;
