----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/02/2019 12:55:09 PM
-- Design Name: 
-- Module Name: TopSystem - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TopSystem is
    Port ( tsrst_i : in STD_LOGIC;
           tsclk_i : in STD_LOGIC;
           sensor_i : in STD_LOGIC_VECTOR (1 downto 0);
           --bomb_o : out STD_LOGIC_VECTOR (1 downto 0);
           --tsdata_o : out STD_LOGIC_VECTOR(3 downto 0);
           tsen_o : out STD_LOGIC_VECTOR(7 downto 0);
           segments : out STD_LOGIC_VECTOR(7 downto 0));
end TopSystem;

architecture Behavioral of TopSystem is

component TopLevel is
    Port ( tlrst_i : in STD_LOGIC;
           tlclk_i : in STD_LOGIC;
           en_i : in STD_LOGIC_VECTOR (1 downto 0);
           output_o : out STD_LOGIC_VECTOR (1 downto 0);
           tldata_o : out STD_LOGIC_VECTOR(3 downto 0));
end component;

component DivFrec is
    Generic (SIZE : INTEGER := 4); 
    Port ( clk_e : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           clk_s : out STD_LOGIC);
end component;

component RingCounter is
    Generic ( rcSIZE : INTEGER := 8 );
    Port ( rcrst_i : in STD_LOGIC;        -- active high reset
           rcclk_i : in STD_LOGIC;        -- rising edge active clock
                                       -- input bit
           rcoutput : out STD_LOGIC_VECTOR(rcSIZE-1 downto 0));      -- output bit
end component;

component sevensegment is
    Port ( datos : in STD_LOGIC_VECTOR (3 downto 0);
          
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end component;

signal div_clk, div_clk2 : std_logic;
signal div_rst : std_logic;
signal en_o, svsalida : std_logic_vector(7 downto 0);
signal svdatostl, svdatos, datos_aux : std_logic_vector(3 downto 0);
signal bomb_aux : std_logic_vector(2 downto 0);
signal bomb_buff : std_logic_vector(1 downto 0);

begin

tl : TopLevel port map (
    tlclk_i => div_clk,
    tlrst_i => tsrst_i,
    en_i => sensor_i,
    output_o => bomb_buff,
    tldata_o => svdatostl
);

dv : DivFrec generic map (SIZE => 100000000) port map (
    clk_e => tsclk_i,
    rst_i => tsrst_i,
    clk_s => div_clk
);

dv2 : DivFrec generic map (SIZE => 100000) port map (
    clk_e => tsclk_i,
    rst_i => tsrst_i,
    clk_s => div_clk2
);

rc : RingCounter port map (
    rcclk_i => div_clk2,
    rcrst_i => tsrst_i,
    rcoutput => en_o
);



--bomb_o <= bomb_buff;
tsen_o <= not en_o;
--segments <= svsalida;
datos_aux <= svdatostl;

process(en_o, bomb_buff, datos_aux)
begin
    
--    if en_o = "10000000" then
--        if sensor_i = "00" then
--            svdatos <= "1111";
--        elsif sensor_i = "10" then
--            svdatos <= "1110";
--        elsif sensor_i = "11" then
--            svdatos <= "0000";      
--        end if;
        
--    elsif en_o = "00100000" then
--        if bomb_buff = "00" then
--            svdatos <= "1101";
--        elsif bomb_buff = "01" then
--            svdatos <= "1101";
            
--        elsif bomb_buff = "10" then
--            svdatos <= "0001";
--        elsif bomb_buff = "11" then
--            svdatos <= "0001";      
--        end if;
--    elsif en_o = "00100000" then
--        if bomb_buff = "00" then
--            svdatos <= "1101";
--        elsif bomb_buff = "01" then
--            svdatos <= "0010";
        
--        elsif bomb_buff = "10" then
--            svdatos <= "1101";
--        elsif bomb_buff = "11" then
--            svdatos <= "0010";      
--    end if;     
    
 --   end if;
    if en_o = "10000000" and bomb_buff(1) = '1' then
        svdatos <= "0110";
    elsif en_o = "00100000" and bomb_buff(0) = '1' then
        svdatos <= "0011";
    elsif en_o = "00001000" then
        svdatos <= datos_aux;
    else
        svdatos <= "1011";
    end if;

end process;

sv : sevensegment port map (
    datos => svdatos,
    salida => segments --svsalida
);

end Behavioral;
