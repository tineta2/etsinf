----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/02/2019 10:24:40 PM
-- Design Name: 
-- Module Name: TopSystem_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TopSystem_sim is
--  Port ( );
end TopSystem_sim;

architecture Behavioral of TopSystem_sim is

component TopSystem is
    Port ( tsrst_i : in STD_LOGIC;
           tsclk_i : in STD_LOGIC;
           tsen_o : out STD_LOGIC_VECTOR(7 downto 0);
           segments : out STD_LOGIC_VECTOR(7 downto 0);
           sensor_i : in STD_LOGIC_VECTOR (1 downto 0));
           --bomb_o : out STD_LOGIC_VECTOR (1 downto 0);
           --tsdata_o : out STD_LOGIC_VECTOR(3 downto 0));
end component;

signal clk, rst : std_logic;
signal insu : std_logic_vector(1 downto 0);
--signal data : std_logic_vector(3 downto 0);
signal en, seg : std_logic_vector(7 downto 0);

constant CLK_PERIOD : time := 10 ns;

begin

tsys : TopSystem port map (
    tsclk_i => clk,
    tsrst_i => rst,
    sensor_i => insu,
    --bomb_o => b12,
    --tsdata_o => data
    tsen_o => en,
    segments => seg
);

process
    begin
        clk <= '0';
        wait for CLK_PERIOD/2;
        clk <= '1';
        wait for CLK_PERIOD/2;
end process;
    
process
begin
    --rst <= '1'    after 2 ns;
    
    --rst <= '0' after 10 ns;
    
    rst <= '1', '0' after 10ns;
     
    insu <= "00";
    
    wait for CLK_PERIOD;
    
    insu <= "10";
        
      wait for CLK_PERIOD;
        
    insu <= "10";
            
       wait for CLK_PERIOD;
            
            
    insu <= "00";
                
       wait for CLK_PERIOD;
                
                
     insu <= "00";
                    
       wait for CLK_PERIOD;
                    
                    
     insu <= "10";
                        
        wait for CLK_PERIOD;
        
     insu <= "10";
                                
        wait for CLK_PERIOD;
     insu <= "00";
                                
          wait for CLK_PERIOD;
                
     insu <= "11";
                                        
         wait for CLK_PERIOD;
     insu <= "11";
                                                
          wait for CLK_PERIOD;
          
          insu <= "10";
                                             
              wait for CLK_PERIOD;
          insu <= "11";
                                                     
               wait for CLK_PERIOD;     
     insu <= "11";
                                             
              wait for CLK_PERIOD;
          insu <= "10";
                                                     
               wait for CLK_PERIOD;     
               
               
      insu <= "11";
                                                  
                   wait for CLK_PERIOD;
       insu <= "10";
                                                          
       wait for CLK_PERIOD;
       
            insu <= "00";
                                          
           wait for CLK_PERIOD;
            
         insu <= "11";
                                               
                wait for CLK_PERIOD;
                 
            
          insu <= "00";
                                                                 
             wait for CLK_PERIOD;
end process;


end Behavioral;
