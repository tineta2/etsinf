----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/01/2019 04:01:30 PM
-- Design Name: 
-- Module Name: TopLevel_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TopLevel_sim is
--  Port ( );
end TopLevel_sim;

architecture Behavioral of TopLevel_sim is

component TopLevel is
    Port ( tlrst_i : in STD_LOGIC;
           tlclk_i : in STD_LOGIC;
           en_i : in STD_LOGIC_VECTOR (1 downto 0);
           output_o : out STD_LOGIC_VECTOR(1 downto 0);
           tldata_o : out STD_LOGIC_VECTOR(3 downto 0));
end component;

signal clk, rst : std_logic;
signal insu, b12 : std_logic_vector(1 downto 0);
signal data : std_logic_vector(3 downto 0);

constant CLK_PERIOD : time := 10 ns;

begin

maq: TopLevel port map (
    tlclk_i => clk,
    tlrst_i => rst,
    en_i => insu,
    output_o => b12,
    tldata_o => data
);

process
    begin
        clk <= '0';
        wait for CLK_PERIOD/2;
        clk <= '1';
        wait for CLK_PERIOD/2;
    end process;
    
process
begin
    --rst <= '1'    after 2 ns;
    
    --rst <= '0' after 10 ns;
    
    rst <= '1', '0' after 10ns;
     
    insu <= "00";
    
    wait for CLK_PERIOD;
    
    insu <= "10";
        
      wait for CLK_PERIOD;
        
    insu <= "10";
            
       wait for CLK_PERIOD;
            
            
    insu <= "00";
                
       wait for CLK_PERIOD;
                
                
     insu <= "00";
                    
       wait for CLK_PERIOD;
                    
                    
     insu <= "10";
                        
        wait for CLK_PERIOD;
        
     insu <= "10";
                                
        wait for CLK_PERIOD;
     insu <= "00";
                                
          wait for CLK_PERIOD;
                
     insu <= "11";
                                        
         wait for CLK_PERIOD;
     insu <= "11";
                                                
          wait for CLK_PERIOD;
          
          insu <= "10";
                                             
              wait for CLK_PERIOD;
          insu <= "11";
                                                     
               wait for CLK_PERIOD;     
     insu <= "11";
                                             
              wait for CLK_PERIOD;
          insu <= "10";
                                                     
               wait for CLK_PERIOD;     
               
               
      insu <= "11";
                                                  
                   wait for CLK_PERIOD;
       insu <= "10";
                                                          
       wait for CLK_PERIOD;
       
            insu <= "00";
                                          
           wait for CLK_PERIOD;
            
         insu <= "11";
                                               
                wait for CLK_PERIOD;
                 
            
          insu <= "00";
                                                                 
             wait for CLK_PERIOD;
end process;

end Behavioral;
