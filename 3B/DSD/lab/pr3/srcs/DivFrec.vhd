----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.03.2019 11:33:21
-- Design Name: 
-- Module Name: DivFrec - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DivFrec is
    Generic (dfSIZE : INTEGER := 4); 
    Port ( dfclk_e : in STD_LOGIC;
           dfrst : in STD_LOGIC;
           dfclk_s : out STD_LOGIC);
end DivFrec;

architecture Behavioral of DivFrec is
signal count : integer := 0;
begin

process(dfclk_e, dfrst)
begin

    if dfrst = '1' then
        count <= 0;
        dfclk_s <= '0';
    elsif dfclk_e'event and dfclk_e = '1' then
        if count < dfSIZE-1 then
            count <= count+1;
            dfclk_s <= '0';
        else
            dfclk_s <= '1';
            count <= 0;
        end if;
    end if;


end process;

end Behavioral;
