(9.5/10.0)

- Diseño (7.0/7.5)
    - Se utiliza la salida del divisor de frecuencia como señal de reloj de los contadores, no como señal de habilitación.
    - Se utiliza la señal que genera el contador de píxeles como señal de reloj y no como señal de habilitación.

- Implementación (2.5/2.5)

- Bonus: Imagen en BRAM IP Core (+0.0/+2.0)
    - No implementado.
