----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.02.2019 12:33:50
-- Design Name: 
-- Module Name: Sum_Rest_nbits - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sum_Rest_nbits is
    Generic ( srSIZE : integer:=4 );
    Port ( OPsr : in STD_LOGIC;
           Asr : in STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           Bsr : in STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           Coutsr : out STD_LOGIC;
           Ssr : out STD_LOGIC_VECTOR (srSIZE-1 downto 0);
           vsr : out STD_LOGIC);
end Sum_Rest_nbits;

architecture Behavioral of Sum_Rest_nbits is

component Full_Adder is
 Port ( A : in STD_LOGIC;
          B : in STD_LOGIC;
          Cin : in STD_LOGIC;
          S : out STD_LOGIC;
          faCout : out STD_LOGIC);
end component;

signal ch : STD_LOGIC_vector (srSIZE-1 downto 0);

signal Bin: STD_LOGIC_vector (srSIZE-1 downto 0);


begin

Bin(0) <= Bsr(0) xor OPsr;

Full_Adder0: Full_Adder port map ( A => Asr(0),
                         B => Bin(0),
                         Cin => OPsr,
                           S => Ssr(0),
                           faCout => ch(0) );

fa: for i in 1 to srSIZE-2 generate
begin

Bin(i) <= Bsr(i) xor OPsr;

fa: Full_Adder port map ( A => Asr(i),
                         B => Bin(i),
                         Cin => ch(i-1),
                         S => Ssr(i),
                           faCout => ch(i));
end generate;

Bin(srSIZE-1) <= Bsr(srSIZE-1) xor OPsr;

Full_AdderN: Full_Adder port map ( A => Asr(srSIZE-1),
                         B => Bin(srSIZE-1),
                         Cin => ch(srSIZE-2),
                           S => Ssr(srSIZE-1),
                           faCout => ch(srSIZE-1));

Coutsr <= ch(srSIZE-1);

vsr <= ch(srSIZE - 1) xor ch(srSIZE-2);

end Behavioral;
