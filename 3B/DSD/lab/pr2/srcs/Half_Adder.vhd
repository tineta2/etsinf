----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.02.2019 11:41:24
-- Design Name: 
-- Module Name: Half_Adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Half_Adder is
--  Port ( );

Port ( A1 : in STD_LOGIC;
       B1 : in STD_LOGIC;
       S1 : out STD_LOGIC;
       Cout1 : out STD_LOGIC );
       
end Half_Adder;



architecture Behavioral of Half_Adder is

begin

S1 <= A1 xor B1;
Cout1 <= A1 and B1;

end Behavioral;
