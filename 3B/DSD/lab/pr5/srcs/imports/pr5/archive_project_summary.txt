***************************************************************************************
*                      PROJECT ARCHIVE SUMMARY REPORT
*
*                      (archive_project_summary.txt)
*
*  PLEASE READ THIS REPORT TO GET THE DETAILED INFORMATION ABOUT THE PROJECT DATA THAT
*  WAS ARCHIVED FOR THE CURRENT PROJECT
*
* The report is divided into following five sections:-
*
* Section (1) - PROJECT INFORMATION
*  This section provides the details of the current project that was archived
*
* Section (2) - INCLUDED/EXCLUDED RUNS
*  This section summarizes the list of design runs for which the results were included
*  or excluded from the archive
*
* Section (3) - ARCHIVED SOURCES
*  This section summarizes the list of files that were added to the archive
*
* Section (3.1) - INCLUDE FILES
*  This section summarizes the list of 'include' files that were added to the archive
*
* Section (3.1.1) - INCLUDE_DIRS SETTINGS
*  This section summarizes the 'verilog include directory' path settings, if any
*
* Section (3.2) - REMOTE SOURCES
*  This section summarizes the list of referenced 'remote' files that were 'imported'
*  into the archived project
*
* Section (3.3) - SOURCES SUMMARY
*  This section summarizes the list of all the files present in the archive
*
* Section (3.4) - REMOTE IP DEFINITIONS
*  This section summarizes the list of all the remote IP's present in the archive
*
* Section (4) - JOURNAL/LOG FILES
*  This section summarizes the list of journal/log files that were added to the archive
*
***************************************************************************************

Section (1) - PROJECT INFORMATION
---------------------------------
Name      = pr5
Directory = /home/cristina/DSD/pr5

WARNING: Please verify the compiled library directory path for the following property in the
         current project. The path may point to an invalid location after opening this project.
         This could happen if the project was unarchived in a location where this path is not
         accessible. To resolve this issue, please set this property with the desired path
         before launching simulation:-

Property = compxlib.xsim_compiled_library_dir
Path     = 

Section (2) - INCLUDED RUNS
---------------------------
The run results were included for the following runs in the archived project:-

<synth_1>
<mem_mips_synth_1>
<impl_1>

Section (3) - ARCHIVED SOURCES
------------------------------
The following sub-sections describes the list of sources that were archived for the current project:-

Section (3.1) - INCLUDE FILES
-----------------------------
List of referenced 'RTL Include' files that were 'imported' into the archived project:-

None

Section (3.1.1) - INCLUDE_DIRS SETTINGS
---------------------------------------
List of the "INCLUDE_DIRS" fileset property settings that may or may not be applicable in the archived
project, since most the 'RTL Include' files referenced in the original project were 'imported' into the
archived project.

<sources_1> fileset RTL include directory paths (INCLUDE_DIRS):-
None

<sim_1> fileset RTL include directory paths (INCLUDE_DIRS):-
None

Section (3.2) - REMOTE SOURCES
------------------------------
List of referenced 'remote' design files that were 'imported' into the archived project:-

<mem_mips>
/home/cristina/DSD/pr5/Bubblesort.coe

<constrs_1>
None

<sim_1>
None

<sources_1>
/home/cristina/DSD/pr5/Bubblesort.coe

Section (3.3) - SOURCES SUMMARY
-------------------------------
List of all the source files present in the archived project:-

<sources_1>
./pr5.srcs/sources_1/new/ALU.vhd
./pr5.srcs/sources_1/new/ALUControl.vhd
./pr5.srcs/sources_1/new/InstructionRegister.vhd
./pr5.srcs/sources_1/new/Memory.vhd
./pr5.srcs/sources_1/new/Register8n.vhd
./pr5.srcs/sources_1/new/RegisterFile.vhd
./pr5.srcs/sources_1/new/automatacontrol.vhd
./pr5.srcs/sources_1/new/shifleft2.vhd
./pr5.srcs/sources_1/new/MiniMIPS.vhd
./pr5.srcs/sources_1/imports/pr5/Bubblesort.coe

<constrs_1>
None

<sim_1>
./pr5.srcs/sim_1/new/MiniMIPS_sim.vhd

<mem_mips>
./pr5.srcs/sources_1/ip/mem_mips/mem_mips.xci
./pr5.srcs/sources_1/ip/mem_mips/doc/blk_mem_gen_v8_4_changelog.txt
./pr5.srcs/sources_1/ip/mem_mips/mem_mips.vho
./pr5.srcs/sources_1/ip/mem_mips/mem_mips.veo
./pr5.srcs/sources_1/ip/mem_mips/summary.log
./pr5.srcs/sources_1/ip/mem_mips/misc/blk_mem_gen_v8_4.vhd
./pr5.srcs/sources_1/ip/mem_mips/mem_mips.mif
./pr5.srcs/sources_1/ip/mem_mips/simulation/blk_mem_gen_v8_4.v
./pr5.srcs/sources_1/ip/mem_mips/sim/mem_mips.v
./pr5.srcs/sources_1/ip/mem_mips/mem_mips.dcp
./pr5.srcs/sources_1/ip/mem_mips/mem_mips_stub.v
./pr5.srcs/sources_1/ip/mem_mips/mem_mips_stub.vhdl
./pr5.srcs/sources_1/ip/mem_mips/mem_mips_sim_netlist.v
./pr5.srcs/sources_1/ip/mem_mips/mem_mips_sim_netlist.vhdl
./pr5.srcs/sources_1/ip/mem_mips/mem_mips_ooc.xdc
./pr5.srcs/sources_1/ip/mem_mips/hdl/blk_mem_gen_v8_4_vhsyn_rfs.vhd
./pr5.srcs/sources_1/ip/mem_mips/synth/mem_mips.vhd
./pr5.srcs/sources_1/ip/mem_mips/mem_mips.xml
./pr5.srcs/sources_1/imports/pr5/Bubblesort.coe

Section (3.4) - REMOTE IP DEFINITIONS
-------------------------------------
List of all the remote IP's present in the archived project:-

<sources_1>
None

<mem_mips>
None

Section (4) - JOURNAL/LOG FILES
-------------------------------
List of Journal/Log files that were added to the archived project:-

Source File = /home/cristina/vivado.jou
Archived Location = ./pr5/vivado.jou

Source File = /home/cristina/vivado.log
Archived Location = ./pr5/vivado.log

