----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/15/2019 11:57:33 AM
-- Design Name: 
-- Module Name: RegisterFile - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RegisterFile is
    Port ( ReadReg1_i : in STD_LOGIC_VECTOR(4 downto 0);
           ReadReg2_i : in STD_LOGIC_VECTOR(4 downto 0);
           WriteReg_i : in STD_LOGIC_VECTOR(4 downto 0);
           WriteData_i : in STD_LOGIC_VECTOR(7 downto 0);
           clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           RegWrite_i : in STD_LOGIC;
           ReadData1_o : out STD_LOGIC_VECTOR(7 downto 0);
           ReadData2_o : out STD_LOGIC_VECTOR(7 downto 0));
end RegisterFile;

architecture Behavioral of RegisterFile is

    type memory is array (0 to 31) of STD_LOGIC_VECTOR(7 downto 0);
    
    signal memory_content : memory := (
       others => "00000000"
    );

begin

process(clk_i, rst_i)
begin
	if rst_i = '1' then
	   ReadData1_o <= "00000000";
	   ReadData1_o <= "00000000";
	elsif clk_i'event and clk_i = '1' and en_i = '1' then
		if RegWrite_i = '1' then
	       memory_content(to_integer(unsigned(WriteReg_i))) <= WriteData_i;
	    else
	       ReadData1_o <= memory_content(to_integer(unsigned(ReadReg1_i)));
	       ReadData2_o <= memory_content(to_integer(unsigned(ReadReg2_i)));
		end if;	
	end if;
end process;


end Behavioral;
