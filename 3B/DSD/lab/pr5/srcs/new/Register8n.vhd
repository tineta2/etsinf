----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/04/2019 11:36:38 AM
-- Design Name: 
-- Module Name: Register8n - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Register8n is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           d_i : in STD_LOGIC_VECTOR (7 downto 0);
           q_o : out STD_LOGIC_VECTOR (7 downto 0));
end Register8n;

architecture Behavioral of Register8n is

begin

process(clk_i, rst_i)
begin
    if rst_i = '1' then
        q_o <= "00000000";
    elsif clk_i'event and clk_i = '1' and en_i = '1' then
        q_o <= d_i; 
    end if;
end process;

end Behavioral;
