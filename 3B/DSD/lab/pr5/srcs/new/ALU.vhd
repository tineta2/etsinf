----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/08/2019 12:14:25 PM
-- Design Name: 
-- Module Name: ALU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALU is
    Port ( a_i : in STD_LOGIC_VECTOR (7 downto 0);
           b_i : in STD_LOGIC_VECTOR (7 downto 0);
           op_i : in STD_LOGIC_VECTOR (2 downto 0);
           s_o : out STD_LOGIC_VECTOR (7 downto 0);
           zero_o : out STD_LOGIC);
end ALU;



architecture Behavioral of ALU is

signal aux : unsigned(7 downto 0);
signal aux1 : unsigned(7 downto 0) := "00000001";
signal aux_o : std_logic_vector(7 downto 0);

begin

process(a_i, b_i, op_i)
begin
case (op_i) is
    when "010" => aux_o <= std_logic_vector(unsigned(a_i) + unsigned(b_i)); 
    when "110" => aux_o <= std_logic_vector(unsigned(a_i) - unsigned(b_i)); 
    when "000" => aux_o <= std_logic_vector(unsigned(a_i) and unsigned(b_i));
    when "001" => aux_o <= std_logic_vector(unsigned(a_i) or unsigned(b_i));
    when "111" =>
        
        if a_i < b_i then
            aux_o <= "00000001";
        else
            aux_o <= "00000000";
        end if;
    when others => 
        aux_o <= std_logic_vector(unsigned(a_i) + unsigned(b_i));
end case;

end process;

process(aux_o)
begin
if aux_o = "00000000" then
    zero_o <= '1';
else
    zero_o <= '0';
end if;
end process;
s_o <= aux_o;

end Behavioral;
