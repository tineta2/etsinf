----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.06.2019 15:24:18
-- Design Name: 
-- Module Name: TopLevel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TopLevel is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           endis_o : out STD_LOGIC_VECTOR(7 downto 0);
           segments : out STD_LOGIC_VECTOR(7 downto 0));
end TopLevel;

architecture Behavioral of TopLevel is

component MiniMIPS is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           port0 : out STD_LOGIC_VECTOR (7 downto 0);
           port1 : out STD_LOGIC_VECTOR (7 downto 0);
           port2 : out STD_LOGIC_VECTOR (7 downto 0);
           port3 : out STD_LOGIC_VECTOR (7 downto 0);
           port4 : out STD_LOGIC_VECTOR (7 downto 0);
           port5 : out STD_LOGIC_VECTOR (7 downto 0);
           port6 : out STD_LOGIC_VECTOR (7 downto 0);
           port7 : out STD_LOGIC_VECTOR (7 downto 0)
           );
end component;

component RingCounter is
    Generic ( rcSIZE : INTEGER := 8 );
    Port ( rcrst_i : in STD_LOGIC;        -- active high reset
           rcclk_i : in STD_LOGIC;        -- rising edge active clock
                                       -- input bit
           en_i : in STD_LOGIC;
           rcoutput : out STD_LOGIC_VECTOR(rcSIZE-1 downto 0));      -- output bit
end component;

component DivFrec is
    Generic (SIZE : INTEGER := 8); 
    Port ( clk_e : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           clk_s : out STD_LOGIC);
end component;

component Display is
    Port ( datos : in STD_LOGIC_VECTOR (3 downto 0);
           salida : out STD_LOGIC_VECTOR (7 downto 0));
end component;

signal dvout1, dvout2 : std_logic;
signal data : std_logic_vector(3 downto 0);
signal en : std_logic_vector(7 downto 0);
--signal clk, rst, en : std_logic;
signal p0, p1, p2, p3, p4, p5, p6, p7 : std_logic_vector(7 downto 0);
signal segnot : std_logic_vector(7 downto 0);

begin

min_mips : MiniMIPS port map (
    clk_i => clk_i,
    rst_i => rst_i,
    en_i => dvout1,
    port0 => p0,
    port1 => p1,
    port2 => p2,
    port3 => p3,
    port4 => p4,
    port5 => p5,
    port6 => p6,
    port7 => p7
);

rc: RingCounter port map (
    rcclk_i => clk_i,
    rcrst_i => rst_i,
    en_i => dvout2,
    rcoutput => en
);

--mips
df1: DivFrec 
    generic map (SIZE => 1000000)
    --generic map (SIZE => 2) 
    port map (
    clk_e => clk_i,
    rst_i => rst_i,
    clk_s => dvout1
);

--ringcounter
df2 : DivFrec 
    generic map (SIZE => 100000) 
    --generic map (SIZE => 4)
    port map (
    clk_e => clk_i,
    rst_i => rst_i,
    clk_s => dvout2
);

dspl : Display port map (
    datos => data,
    salida => segments
);

endis_o <= not en;

process(en)
begin
    if en = "10000000" then
        data <= p0(3 downto 0);
    elsif en = "01000000" then
        data <= p1(3 downto 0);
    elsif en = "00100000" then
        data <= p2(3 downto 0);
    elsif en = "00010000" then
        data <= p3(3 downto 0);
    elsif en = "00001000" then
        data <= p4(3 downto 0);
    elsif en = "00000100" then
        data <= p5(3 downto 0);
    elsif en = "00000010" then
        data <= p6(3 downto 0);
    elsif en = "00000001" then
        data <= p7(3 downto 0);
    end if;
end process;

end Behavioral;
