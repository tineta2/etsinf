----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.05.2019 13:48:36
-- Design Name: 
-- Module Name: ALUcontrol - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ALUcontrol is
    Port ( instruction : in STD_LOGIC_VECTOR (5 downto 0);
           ALUop : in STD_LOGIC_VECTOR (1 downto 0);
           ALUcontrol : out STD_LOGIC_VECTOR (2 downto 0));
end ALUcontrol;

architecture Behavioral of ALUcontrol is

begin

process(instruction, ALUop)
begin

if ALUop = "00" then 
    ALUcontrol <= "010";
elsif ALUop = "01" then
    ALUcontrol <= "110";
elsif ALUop = "10" then

    if instruction = "100000" then
        ALUcontrol <= "010";
    elsif instruction = "100010" then
        ALUcontrol <= "110";
    elsif instruction = "100100" then
        ALUcontrol <= "000";
    elsif instruction = "100101" then
        ALUcontrol <= "001";
    elsif instruction = "101010" then
        ALUcontrol <= "111";
    end if;
    
end if;
end process;

end Behavioral;
