----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/03/2019 08:22:50 PM
-- Design Name: 
-- Module Name: Memory - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Memory is
    Port ( clk_i : in STD_LOGIC;
           rst_i : in STD_LOGIC;
           en_i : in STD_LOGIC;
           MemWrite_i : in STD_LOGIC;
           Address_i : in STD_LOGIC_VECTOR(7 downto 0);
           WriteData_i : in STD_LOGIC_VECTOR(7 downto 0);
           MemData_o : out STD_LOGIC_VECTOR(7 downto 0);
           --Mem_o : out STD_LOGIC;
           port0 : out STD_LOGIC_VECTOR(7 downto 0);
           port1 : out STD_LOGIC_VECTOR(7 downto 0);
           port2 : out STD_LOGIC_VECTOR(7 downto 0);
           port3 : out STD_LOGIC_VECTOR(7 downto 0);
           port4 : out STD_LOGIC_VECTOR(7 downto 0);
           port5 : out STD_LOGIC_VECTOR(7 downto 0);
           port6 : out STD_LOGIC_VECTOR(7 downto 0);
           port7 : out STD_LOGIC_VECTOR(7 downto 0));
end Memory;

architecture Behavioral of Memory is

COMPONENT mem_mips
  PORT (
    clka : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
  );
END COMPONENT;

type memory is array (248 to 255) of STD_LOGIC_VECTOR(7 downto 0);
    
    signal memory_content : memory := (
       others => "00000000"
    );

begin

ip_core : mem_mips port map (
    clka => clk_i,
    ena => en_i,
    wea(0) => MemWrite_i,
    addra => Address_i,
    dina => WriteData_i,
    douta => MemData_o
);

process(clk_i, rst_i)
begin
    if rst_i = '1' then
        memory_content <= (others => "00000000");
    elsif clk_i'event and clk_i = '1' and en_i = '1' and MemWrite_i = '1' then
        if Address_i >= std_logic_vector(to_unsigned(248, 8)) and Address_i <= std_logic_vector(to_unsigned(255, 8)) then
            memory_content(to_integer(unsigned(Address_i))) <= WriteData_i;
        end if;
    end if;
end process; 

port0 <= memory_content(248);
port1 <= memory_content(249);
port2 <= memory_content(250);
port3 <= memory_content(251);
port4 <= memory_content(252);
port5 <= memory_content(253);
port6 <= memory_content(254);
port7 <= memory_content(255);

end Behavioral;
