(9.9/10.0)

- Memoria (1.0/1.0)

- Registro de instrucción (1.0/1.0)

- Banco de registros (1.0/1.0)

- ALU y ALU Control (0.9/1.0)
    - Se genera un latch al mantener el valor de la señal ALUcontrol (falta cláusula else) en ALUControl.

- FSM (2.0/2.0)

- Diseño estructural (2.0/2.0)

- Simulación (1.0/1.0)

- Implementación (1.0/1.0)    
    - Se genera un latch al mantener el valor de la señal data (falta cláusula else) en TopLevel.
