#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
MODULE_LICENSE("Dual BSD/GPL");
EXPORT_SYMBOL(acumular, llevamos);

//int n = 1;
//module_param(n, int, S_IRUGO);
int suma = 0;

static int __init entrando(void) {
  unsigned long t_ins = get_seconds();
  printk(KERN_NOTICE "Instante de inserción: %lu\n",t_ins);
  return 0;
}

static void __exit saliendo(void) {
  unsigned long t_ext = get_seconds();
  printk(KERN_NOTICE "Instante de extracción: %lu\n", t_ext);
}

void acumular(int i) {
  suma += i;
}

int llevamos(void) {
  return suma;
}

module_init(entrando);
module_exit(saliendo);
