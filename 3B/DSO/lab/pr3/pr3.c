#ifndef __KERNEL__
#  define __KERNEL__
#endif
#ifndef MODULE
#  define MODULE
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
MODULE_LICENSE("Dual BSD/GPL");

/*Parametros generales*/
#define MAJOR_NUM 55
#define DIR_BASE 0xc030
#define DEV_NAME "/dev/practica"
#define PORT_NAME "parallel"





/*entry module*/


/*funciones de acceso a los puertos*/
//request_region (unsigned long port, unsigned long range, const char *name)
//void release_region (unsigned long port, unsigned long range)

/*funciones de manejo de dispositivo*/

int practica_open(struct inode *inode, struct file *filp)
{
  //para indicar que un proceso esta usando el modulo
  try_module_get(THIS_MODULE);


  return 0; //si todo ha ido bien
}

int practica_release(struct inode *inode, struct file *filp)
{
  //decrementar contador de dispositivos abiertos
  module_put(THIS_MODULE);

  return 0; //si todo ha ido bien
}

/*Debe leer del dispositivo y enviar info a proceso demandante*/
ssize_t practica_read(struct file *filp, char *buf, size_t count, loff_t *offp)
{
  //averiguar estado de microinterruptores y devolverlo como cadena
  //de 5B en buf

  char state = inb(DIR_BASE+1); //puerto de estado
  char cad[5] = "0000\n";
  
  //pin13 - S4
  cad[0] = ((state & 0b00010000))?'1':'0';
  //pin12 - S5
  cad[1] = ((state & 0b00100000))?'1':'0';
  //pin11 - (not)S7
  cad[2] = ((state & 0b10000000))?'0':'1';
  //pin10 - S6
  cad[3] = ((state & 0b01000000))?'1':'0';

  copy_to_user(buf,cad,5);  

  printk(KERN_INFO "Estado de los microinterruptores: %s\n", cad);
  
  return 5;
}

ssize_t practica_write(struct file *filp, const char *buf, size_t count, loff_t *offp)
{
  
  char c;
  
  copy_from_user(&c,buf,1);
  
  if (c=='0') 
    outb(0b00111111, DIR_BASE); //puerto de datos
  else if(c=='1')
	outb(0b00000110, DIR_BASE);
  else if(c=='2')
	outb(0b01011011, DIR_BASE);
  else if(c=='3')
	outb(0b01001111, DIR_BASE);
  else if(c=='4')
	outb(0b01100111, DIR_BASE);
  else if(c=='5')
	outb(0b01101101, DIR_BASE);
  else if(c=='6')
	outb(0b01111100, DIR_BASE);
  else if(c=='7')
	outb(0b00100111, DIR_BASE);	
  else if(c=='8')
	outb(0b01111111, DIR_BASE);
  else if(c=='9')
	outb(0b01101111, DIR_BASE);
  else if (c == '.')
	outb(0b10000000, DIR_BASE);
  else
	outb(0b00000000, DIR_BASE);
    
  return 1;
}

/*
*Pasamos este parametro a register_chrdev()
*los campos indican a que funciones llamar cuando se producen
*las 4 operaciones sobre un fichero especial de dispositivo que
*tenga el major number correspondiente
*/

struct file_operations practica_fops = {
 .read= practica_read,
 .write= practica_write,
 .open= practica_open,
 .release= practica_release,
};

static int __init entrando(void)
{

  //registrar major number
          //devuelve >=0 si se registra bien
  if (register_chrdev(MAJOR_NUM,DEV_NAME,&practica_fops) < 0) {
      printk(KERN_INFO "Error: major number no disponible.\n");
  }


  //hay que comprobar la disponibilidad del rango de puertos
  if (request_region(DIR_BASE, 2, PORT_NAME) == NULL) {
    printk(KERN_INFO "Error: rango de puertos no disponible.\n");
  }

  return 0;
}

/*exit module*/
static void __exit saliendo(void)
{
  unregister_chrdev(MAJOR_NUM,DEV_NAME); //unregister major number
                        //se le pasa major number y nombre
  //tambien hay que liberar el rango de puertos reservados
  release_region(DIR_BASE, 2);
}

/*funciones de acceso a mem del proceso usuario desde nucleo
*unsigned long copy_from_user(void *to, const void *from, unsigned long n)
*unsigned long copy_to_user(void *to, const void *from, unsigned long n)
*/

module_init(entrando);
module_exit(saliendo);
