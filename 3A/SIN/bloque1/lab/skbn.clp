(defglobal ?*nod-gen* = 0)

(deffunction inicio ()
        (reset)
	(printout t "Profundidad Maxima:= " )
	(bind ?prof (read))
	(printout t "Tipo de Busqueda " crlf "    1.- Anchura" crlf "    2.- Profundidad" crlf )
	(bind ?a (read))
	(if (= ?a 1)
	       then    (set-strategy breadth)
	       else   (set-strategy depth))
        (printout t " Ejecuta run para poner en marcha el programa " crlf)
	(assert (grid 5 8))
	(assert (obs 1 4))
	(assert (obs 3 1))
	(assert (obs 3 4))
	(assert (obs 3 5))
	(assert (obs 3 8))
	(assert (obs 4 4))
	;;(assert (obs 4 2))
	(assert (obs 5 4))
	(assert (robot 1 1 cajas c 2 2 c 2 6 c 4 3 almacenes a 1 7 0 a 4 5 0 a 5 5 0 nivel 0))
	(assert (profundidad-maxima ?prof))
)


(defrule derecha
	(robot ?rf ?rc cajas $?cajas almacenes $?almacenes nivel ?nivel)
	(grid ?gf ?gc)
	(test (< ?rc ?gc))
	(not (obs ?rf =(+ ?rc 1))) ;;= -> precalcular valor
	(test (not (member$ (create$ c ?rf (+ ?rc 1)) $?cajas)))
	(test (not (member$ (create$ a ?rf (+ ?rc 1)) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot ?rf (+ ?rc 1) cajas $?cajas almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule izquierda
	(robot ?rf ?rc cajas $?cajas almacenes $?almacenes nivel ?nivel)
	(grid ?gf ?gc)
	(test (> ?rc 1))
	(not (obs ?rf =(- ?rc 1)))
	(test (not (member$ (create$ c ?rf (- ?rc 1)) $?cajas)))
	(test (not (member$ (create$ a ?rf (- ?rc 1)) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot ?rf (- ?rc 1) cajas $?cajas almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule arriba
	(robot ?rf ?rc cajas $?cajas almacenes $?almacenes nivel ?nivel)
	(test (> ?rf 1))
	(not (obs =(- ?rf 1) ?rc))
	(test (not (member$ (create$ c (- ?rf 1) ?rc) $?cajas)))
	(test (not (member$ (create$ a (- ?rf 1) ?rc) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot (- ?rf 1) ?rc cajas $?cajas almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule abajo
	(robot ?rf ?rc cajas $?cajas almacenes $?almacenes nivel ?nivel)
	(grid ?gf ?gc)
	(test (< ?rf ?gf))
	(not (obs =(+ ?rf 1) ?rc))
	(test (not (member$ (create$ c (+ ?rf 1) ?rc) $?cajas)))
	(test (not (member$ (create$ a (+ ?rf 1) ?rc) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot (+ ?rf 1) ?rc cajas $?cajas almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujarderecha
	?f <- (robot ?rf ?rc cajas $?cajas1 c ?rf =(+ ?rc 1) $?cajas2 almacenes $?almacenes nivel ?nivel)
	(grid ?gf ?gc)
	(test (< (+ ?rc 1) ?gc))
	(not (obs ?rf =(+ ?rc 2)))
	(test (not (member$ (create$ c ?rf (+ ?rc 2)) $?cajas1)))
	(test (not (member$ (create$ c ?rf (+ ?rc 2)) $?cajas2)))
	(test (not (member$ (create$ a ?rf (+ ?rc 2)) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot ?rf (+ ?rc 1) cajas $?cajas1 c ?rf (+ ?rc 2) $?cajas2 almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujarizquierda
	?f <- (robot ?rf ?rc cajas $?cajas1 c ?rf =(- ?rc 1) $?cajas2 almacenes $?almacenes nivel ?nivel)
	(test (> (- ?rc 1) 1))
	(not (obs ?rf =(- ?rc 2)))
	(test (not (member$ (create$ c ?rf (- ?rc 2)) $?cajas1)))
	(test (not (member$ (create$ c ?rf (- ?rc 2)) $?cajas2)))
	(test (not (member$ (create$ a ?rf (- ?rc 2)) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot ?rf (- ?rc 1) cajas $?cajas1 c ?rf (- ?rc 2) $?cajas2 almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujararriba
	?f <- (robot ?rf ?rc cajas $?cajas1 c =(- ?rf 1) ?rc $?cajas2 almacenes $?almacenes nivel ?nivel)
	(grid ?gf ?gc)
	(test (> (- ?rf 1) 1))
	(not (obs =(- ?rf 2) ?rc))
	(test (not (member$ (create$ c (- ?rf 2) ?rc) $?cajas1)))
	(test (not (member$ (create$ c (- ?rf 2) ?rc) $?cajas2)))
	(test (not (member$ (create$ a (- ?rf 2) ?rc) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot (- ?rf 1) ?rc cajas $?cajas1 c (- ?rf 2) ?rc $?cajas2 almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujarabajo
	?f <- (robot ?rf ?rc cajas $?cajas1 c =(+ ?rf 1) ?rc $?cajas2 almacenes $?almacenes nivel ?nivel)
	(grid ?gf ?gc)
	(test (> ?gf (+ ?rf 1)))
	(not (obs =(+ ?rf 2) ?rc))
	(test (not (member$ (create$ c (+ ?rf 2) ?rc) $?cajas1)))
	(test (not (member$ (create$ c (+ ?rf 2) ?rc) $?cajas2)))
	(test (not (member$ (create$ a (+ ?rf 2) ?rc) $?almacenes)))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot (+ ?rf 1) ?rc cajas $?cajas1 c (+ ?rf 2) ?rc $?cajas2 almacenes $?almacenes nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujaralmacenderecha
	?f <- (robot ?rf ?rc cajas $?cajas1 c ?rf =(+ ?rc 1) $?cajas2 almacenes $?almacenes1 a ?rf =(+ ?rc 2) ?numcajas $?almacenes2 nivel ?nivel)
	(test (= ?numcajas 0))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot ?rf (+ ?rc 1) cajas $?cajas1 $?cajas2 almacenes $?almacenes1 a ?rf (+ ?rc 2) (+ ?numcajas 1) $?almacenes2 nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujaralmacenizquierda
	?f <- (robot ?rf ?rc cajas $?cajas1 c ?rf =(- ?rc 1) $?cajas2 almacenes $?almacenes1 a ?rf =(- ?rc 2) ?numcajas $?almacenes2 nivel ?nivel)
	(test (= ?numcajas 0))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot ?rf (- ?rc 1) cajas $?cajas1 $?cajas2 almacenes $?almacenes1 a ?rf (- ?rc 2) (+ ?numcajas 1) $?almacenes2 nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujaralmacenarriba
	?f <- (robot ?rf ?rc cajas $?cajas1 c =(- ?rf 1) ?rc $?cajas2 almacenes $?almacenes1 a =(- ?rf 2) ?rc ?numcajas $?almacenes2 nivel ?nivel)
	(test (= ?numcajas 0))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot (- ?rf 1) ?rc cajas $?cajas1 $?cajas2 almacenes $?almacenes1 a (- ?rf 2) ?rc (+ ?numcajas 1) $?almacenes2 nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule empujaralmacenabajo
	?f <- (robot ?rf ?rc cajas $?cajas1 c =(+ ?rf 1) ?rc $?cajas2 almacenes $?almacenes1 a =(+ ?rf 2) ?rc ?numcajas $?almacenes2 nivel ?nivel)
	(test (= ?numcajas 0))
	(profundidad-maxima ?prof)
	(test (< ?nivel ?prof))
	=>
	(assert (robot (+ ?rf 1) ?rc cajas $?cajas1 $?cajas2 almacenes $?almacenes1 a =(+ ?rf 2) ?rc (+ ?numcajas 1) $?almacenes2 nivel (+ ?nivel 1)))
	(bind ?*nod-gen* (+ ?*nod-gen* 1))
)

(defrule objetivo
	(robot $?robot cajas almacenes $?almacenes nivel ?nivel)
	=>
	(printout t "Objetivo alcanzado " crlf)
	(printout t "Nivel alcanzado: " ?nivel crlf)
	(printout t "Numero de nodos generados: " ?*nod-gen* crlf)
	(halt)
)
