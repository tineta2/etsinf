with Pendulum_IO; use Pendulum_IO;
with Ada.Real_Time; use Ada.Real_Time;

procedure Pendulum_4 is
   Init_Time : Time;
   Next : Time;
   Cycle_Duration : Time_Span;
   Flashing_Time : Time_Span := Microseconds (200);
begin
   loop
      Wait_For_Next_Cycle (Init_Time, Cycle_Duration);
      Next := Init_Time + (Cycle_Duration / 4);
      delay until Next;
      Set_LEDs (16#FF#);
      Next := Next + Flashing_Time;
      delay until Next;
      Clear_All_LEDs;
   end loop;
end Pendulum_4;
