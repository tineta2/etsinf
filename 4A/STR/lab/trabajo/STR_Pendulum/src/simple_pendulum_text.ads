with Pendulum_IO; use Pendulum_IO;
with Chars_8x5; use Chars_8x5;
with Ada.Real_Time; use Ada.Real_Time;
with System; use System;
with Ada.Text_IO; use Ada.Text_IO;

package Simple_Pendulum_Text is

   
     
   -- Size in characters of text to be displayed
   Line_Size : constant := 14;
   
   -- Left and right margins, in number of Bars
   Margin : constant := 20;
   
   -- Number of columns per character
   Bars_Per_Char : constant := 6;
   
   Max : Integer := 1024;
   
   -- Display_String
   -- Displays a string of Line_Size Characters
   -- The Text string is trimmed to Line_Sized chars
   procedure Display_String (Text : in String);

end Simple_Pendulum_Text;
