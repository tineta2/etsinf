with STM32.GPIO; use STM32.GPIO;
with STM32.Device; use STM32.Device;
with Boton_Usuario; use Boton_Usuario;
with Ada.Interrupts.Names;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Real_Time.Timing_Events; use Ada.Real_Time.Timing_Events;
with STM32.EXTI; use STM32.EXTI;
with STM32.SYSCFG; use STM32.SYSCFG;
with System;
with Ada.Synchronous_Task_Control; use Ada.Synchronous_Task_Control;

package Pendulum_IO is
   
   N : constant Integer := 10;
   type Index_Mod is mod N;
   Divisor_Frecuencia : Index_Mod := 0;

   --  Byte type to represent LED patterns
   type Byte is mod 2**8;

   --  Position of bit in byte
   type Bit_Pos is mod 8;
   
   --Switch all pendulum LEDs off
   procedure Clear_All_LEDs  with Inline;
   
   --Switch LEDs on/off using byte pattern (1 => On, 0 => Off)
   procedure Set_LEDs (Pattern : in Byte) with Inline;
   
   --Set individual LED indicated by Pos
   procedure Set_Led (Pos : in Bit_Pos) with Inline;
   
   --Clear individual LED indicated by Pos
   procedure Clear_LED (Pos : in Bit_Pos) with Inline;
   
   -- Get the DivisorFrecuencia
   function Get_Divisor return Index_Mod with Inline;
   
   -- Synchronization point with start of new cycle
   procedure Wait_For_Next_Cycle
     (Init_Time : out Time;
      Cycle_Duration : out Time_Span ) with Inline;
   
private
   --Pendulum LEDs (outputs to pendulum board, top to bottom)
   LED_Points : GPIO_Points (1 .. 8) :=
     (PC4, PB0, PA2, PC2, PA1, PC1, PA3, PC5);
   --Barrier signal (input, coming from optocoupler)
   Barrier_Point : GPIO_Point renames PB8;
   User_Button_Point     : GPIO_Point renames PA0;
   
   --Barrier interruptname
   Barrier_Interrupt : constant Ada.Interrupts.Interrupt_ID :=
     Ada.Interrupts.Names.EXTI9_5_Interrupt;
   
   --Interrupcion Boton
   Interrupcion_Boton : constant Ada.Interrupts.Interrupt_ID := Ada.Interrupts.Names.EXTI0_Interrupt;
   
   
   --Interrupcion_Boton : constant External_Line_Number := User_Button_Point.Interrupt_Line_Number;
   
   -- GPIO that connects with Solenoid
   Solenoid_Point : GPIO_Point renames PB13;
end Pendulum_IO;
