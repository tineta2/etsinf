pragma Ada_2012;

package body Pendulum_IO is

   type FSM_State is (S0, S1, S2, S3, S4);
   Release_P_Controller : Suspension_Object;

   --------------------
   ----- Solenoid -----
   --------------------

   protected Solenoid
     with Priority => System.Interrupt_Priority'Last is

      procedure Set_Off;
      procedure Set_During (Pulse_Width : Time_Span);
      procedure Set_Periodic (Pulse_Width, Period : Time_Span);

   private
      Pulse_TE : Timing_Event;
      Periodic_Pulse_TE : Timing_Event;
      procedure Pulse_Handler (TE : in out Timing_Event);
      procedure Periodic_Pulse_Handler (TE : in out Timing_Event);

      -- Stare variables for periodic pulse trains
      Periodic_Pulse_Width : Time_Span; -- Duration of per. pulse "high"
      Period : Time_Span;               -- Period of periodic pulse
   end Solenoid;

   protected body Solenoid is
      procedure Set_During (Pulse_Width : Time_Span) is
         Now  : constant Time := Clock;
         Test : Boolean;
      begin
         Cancel_Handler (Periodic_Pulse_TE, Test);
         Set (Solenoid_Point);
         Set_Handler (Event => Pulse_TE, At_Time => Now + Pulse_Width,
                      Handler => Pulse_Handler'Access);
      end Set_During;

      procedure Pulse_Handler (TE : in out Timing_Event) is
         pragma Unreferenced (TE);
      begin
         Clear (Solenoid_Point);
      end Pulse_Handler;

      procedure Set_Periodic (Pulse_Width, Period : Time_Span) is
         Now  : constant Time := Clock;
         Test : Boolean;

      begin
         Cancel_Handler (Pulse_TE, Test);
         Periodic_Pulse_Width := Pulse_Width;
         Solenoid.Period := Period;
         Set (Solenoid_Point);
         Set_Handler (Event => Periodic_Pulse_TE,
                      At_Time => Now + Pulse_Width,
                      Handler => Periodic_Pulse_Handler'Access);
      end Set_Periodic;

      procedure Periodic_Pulse_Handler (TE : in out Timing_Event) is
         Now : constant Time := Clock;
      begin
         if Set (Solenoid_Point) then
            Clear (Solenoid_Point);
            Set_Handler (Event => Periodic_Pulse_TE,
                         At_Time => Now - Periodic_Pulse_Width + Period,
                         Handler => Periodic_Pulse_Handler'Access);
         else
            Set (Solenoid_Point);
            Set_Handler (Event => Periodic_Pulse_TE,
                         At_Time => Now + Periodic_Pulse_Width,
                         Handler => Periodic_Pulse_Handler'Access);
         end if;
      end Periodic_Pulse_Handler;

      procedure Set_Off is
         Test : Boolean;
      begin
         Cancel_Handler (Periodic_Pulse_TE, Test);
         Cancel_Handler (Pulse_TE, Test);
         Clear (Solenoid_Point);
      end Set_Off;
   end Solenoid;

   ---------------
   -- P_Tracker --
   ---------------

   protected P_Tracker is
      pragma Interrupt_Priority;
      entry Wait_For_Next_Cycle (Init_Time : out Time;
                                 Cycle_Duration : out Time_Span);
      function Next_Cycle_Start return Time; -- aux. functions
      function Next_Cycle_Duration return Time_Span;
      function Barrier_Timedout return Boolean; -- exercise 4
   private
      P_State : FSM_State := S0; -- Current pendulum state
      Next_Start : Time; --Start time of next pendulum cycle
      Pendulum_Period : Time_Span; -- Duration of pendulum cycle
      T1, T2, T3, T4, T5 : Time; -- Times of relevant edges of Barrier
      Ending_Cycle : Boolean := False; -- Entry barrier of Wait_For_Next_Cycle
      Timeout_TE : Timing_Event; -- exercise 4
      Timedout : Boolean := True;
      procedure Timeout_Handler (Event : in out Timing_Event);
       -- exercise 4
      procedure Barrier_Interrupt_Handler;
      procedure Manejador_Interrupcion_Boton;
      pragma Attach_Handler (Barrier_Interrupt_Handler,
                             Barrier_Interrupt);
      pragma Attach_Handler (Manejador_Interrupcion_Boton,
                             Interrupcion_Boton);
   end P_Tracker;

   protected body P_Tracker is

      procedure Timeout_Handler (Event : in out Timing_Event) is -- exercise 4
         Now         : Time;
         Ending_Time : Time_Span;
      begin
         Now := Clock;
         Ending_Time := Milliseconds (1000);
         Timedout := True;
         Set_True (Release_P_Controller); -- to continue P_Controller
         Set_Handler (Event => Timeout_TE,
                      At_Time => Now + Ending_Time,
                      Handler => Timeout_Handler'Access);
      end Timeout_Handler;

      procedure Barrier_Interrupt_Handler is
         Now : Time;
         Ending_Time : Time_Span; -- exercise 4
      begin
         Clear_External_Interrupt (Barrier_Point.Pin);
         Now := Clock;
         Ending_Time := Milliseconds (1000); -- exercise 4
         Timedout := False;
         Set_Handler (Event => Timeout_TE,
                      At_Time => Now + Ending_Time,
                      Handler => Timeout_Handler'Access); -- exercise 4
         case P_State is
            when S0 =>
               if Set (Barrier_Point) then
                  T1 := Now;
                  P_State := S1;
               end if;
            when S1 =>
               if not Set (Barrier_Point) then
                  T2 := Now;
                  P_State := S2;
               end if;
            when S2 =>
               if Set (Barrier_Point) then
                  T3 := Now;
                  P_State := S3;
               end if;
            when S3 =>
               if not Set (Barrier_Point) then
                  T4 := Now;
                  P_State := S4;
               end if;
            when S4 =>
               if Set (Barrier_Point) then
                  T5 := Now;
                  if (T3 - T1) > (T5 - T3) then
                     T1 := T3;
                     T2 := T4;
                     T3 := T5;
                     P_State := S3;
                  else
                     Next_Start := T5 + ((T4 - T1) / 2);
                     Pendulum_Period := T5 - T1;
                     Ending_Cycle := True;
                     T1 := T5;
                     P_State := S1;
                     Set_True (Release_P_Controller); -- we know when and how
                     -- long is next cycle
                  end if;
               end if;
            when others =>
               null;
         end case;

      end Barrier_Interrupt_Handler;

      procedure Manejador_Interrupcion_Boton is

      begin
         Clear_External_Interrupt (User_Button_Point.Pin);
         --Clear_External_Interrupt (EXTI_Line);
         Divisor_Frecuencia := Divisor_Frecuencia + 1;
      end Manejador_Interrupcion_Boton;

      entry Wait_For_Next_Cycle (Init_Time : out Time;
                                 Cycle_Duration : out Time_Span) when Ending_Cycle is
      begin
         Init_Time := Next_Start;
         Cycle_Duration := Pendulum_Period;
         Ending_Cycle := False;
      end Wait_For_Next_Cycle;

      function Next_Cycle_Start return Time is (Next_Start);
      function Next_Cycle_Duration return Time_Span is (Pendulum_Period);
      function Barrier_Timedout return Boolean is (Timedout);


      end P_Tracker;



      --------------------
      --- P_Controller ---
      --------------------

      task P_Controller;

      task body P_Controller is
         N_Start : Time;
         N_Time : Time_Span;

         procedure Initial_Sequence is
            Period : Time_Span := Milliseconds (114);
         begin
            Solenoid.Set_Periodic (Period / 2, Period);
            delay until Clock + Milliseconds (3000);
         end Initial_Sequence;

      begin
         loop
            Initial_Sequence;
            loop --exercise 4
               Suspend_Until_True (Release_P_Controller);
               if P_Tracker.Barrier_Timedout = True then
                  exit;
               else
                  N_Start := P_Tracker.Next_Cycle_Start;
               N_Time := P_Tracker.Next_Cycle_Duration;
               N_Start := N_Start + (N_Time / 6);
               delay until N_Start;
               Solenoid.Set_During (Milliseconds (15));
            end if;
         end loop;
      end loop;
   end P_Controller;



   --------------------
   -- Clear_All_LEDs --
   --------------------

   procedure Clear_All_LEDs is
   begin
      Set (LED_Points);
   end Clear_All_LEDs;

   --------------
   -- Set_LEDs --
   --------------

   procedure Set_LEDs (Pattern : in Byte) is
      V : Byte := Pattern; -- Make local copy
   begin
      for Count in 1..8 loop
         if V mod 2 = 0 then
            Set (LED_Points (Count));
         else
            Clear (LED_Points (Count));
         end if;
         V := V / 2;
      end loop;
   end Set_LEDs;

   -------------
   -- Set_Led --
   -------------

   procedure Set_Led (Pos : in Bit_Pos) is
      Aux : Integer := Integer (Pos);
   begin
      Aux := Aux + 1;
      Clear (LED_Points (Aux));
   end Set_Led;

   -----------------
   -- Get_Divisor --
   -----------------

   function Get_Divisor return Index_Mod is (Divisor_Frecuencia);

   ---------------
   -- Clear_LED --
   ---------------

   procedure Clear_LED (Pos : in Bit_Pos) is
      Aux : Integer := Integer (Pos);
   begin
      Aux := Aux + 1;
      Set (LED_Points (Aux));
   end Clear_LED;

   -------------------------
   -- Wait_For_Next_Cycle --
   -------------------------

   procedure Wait_For_Next_Cycle (Init_Time : out Time;
                                  Cycle_Duration : out Time_Span) is
   begin
      P_Tracker.Wait_For_Next_Cycle (Init_Time, Cycle_Duration);
   end Wait_For_Next_Cycle;

   -------------------------
   -- Initialise_Pendulum --
   -------------------------

   procedure Initialise_Pendulum is

   begin
      --  Enable GPIO ports used for the pendulum's LED_Points
      Enable_Clock (LED_Points & Barrier_Point & Solenoid_Point & User_Button_Point);

      --  Configure LED_Points
      Configure_IO (Points => LED_Points & Solenoid_Point,
                    Config => (Mode        => Mode_Out,
                               Resistors   => Floating,
                               Output_Type => Push_Pull,
                               Speed       => Speed_100MHz));

      --  Configuration of additional pendulum pins
      Configure_IO (Barrier_Point & User_Button_Point, (Mode => Mode_In, Resistors => Pull_Up));
      Configure_Trigger (Barrier_Point, Interrupt_Rising_Falling_Edge);

      Configure_IO (User_Button_Point, (Mode => Mode_In, Resistors => Pull_Down));
      Configure_Trigger (User_Button_Point, Interrupt_Rising_Edge);
   end Initialise_Pendulum;

begin
   --Initialize;
   Initialise_Pendulum;
   Clear_All_LEDs;
end Pendulum_IO;
