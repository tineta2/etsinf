with Ada.Real_Time; use Ada.Real_Time;
with STM32.GPIO; use STM32.GPIO;
with STM32.Device; use STM32.Device;

procedure Pendulum_2 is

   --  GPIO_Points of Pendulum LEDs (outputs, top to bottom)
   LED_Points : GPIO_Points (1 .. 8) := (PC4, PB0, PA2, PC2, PA1, PC1, PA3, PC5);

   procedure Initialise_Pendulum is
   begin
      --  Enable GPIO ports used for the pendulum's LED_Points
      Enable_Clock (LED_Points);

      --  Configure LED_Points
      Configure_IO (Points => LED_Points,
                    Config => (Mode        => Mode_Out,
                               Resistors   => Floating,
                               Output_Type => Push_Pull,
                               Speed       => Speed_100MHz));

      --  Configuration of additional pendulum pins
      --  ...

   end Initialise_Pendulum;

   --  Byte type to represent LED patterns
   type Byte is mod 2**8;

   --  Position of bit in byte
   type Bit_Pos is mod 8;

   --  Turn LEDs on/off using byte pattern (1 => On, 0 => Off)
   procedure Set_LEDs (Pattern : in Byte)
     with Inline
   is
      V : Byte := Pattern; -- Make local copy
   begin
      for Count in 1..8 loop
         if V mod 2 = 0 then
            Set (LED_Points (Count));
         else
            Clear (LED_Points (Count));
         end if;
         V := V / 2;
      end loop;
   end Set_LEDs;

   --  Turn all pendulum LEDs off
   procedure Clear_All_LEDs
     with Inline
   is
   begin
      Set (LED_Points);
   end Clear_All_LEDs;


   --  Turn on individual LED indicated by Pos
   procedure Set_LED (Pos : in Bit_Pos)
     with Inline
   is
      Aux : Integer := Integer (Pos);
   begin
      Aux := Aux + 1;
      Clear (LED_Points (Aux));
   end Set_LED;


   --  Turn off individual LED indicated by Pos
   procedure Clear_LED (Pos : in Bit_Pos)
     with Inline
   is
      Aux : Integer := Integer (Pos);
   begin
      Aux := Aux + 1;
      Set (LED_Points (Aux));
   end Clear_LED;


   Pos   : Bit_Pos;
   Value : Byte := 0;

begin

   Initialise_Pendulum;

   ------------------------------
   --  Test for Clear_All_LEDs --
   ------------------------------
   Clear_All_LEDs;

   ------------------------
   --  Test for Set_LED  --
   ------------------------
   Pos := 0;
   for I in 1 .. 8 loop
      delay until Clock + Milliseconds (250);
      Set_LED (Pos);
      Pos := Pos + 1;
   end loop;

   --------------------------
   --  Test for Clear_LED  --
   --------------------------
   Pos := 7;
   for I in 1 .. 8 loop
      delay until Clock + Milliseconds (250);
      Clear_LED (Pos);
      Pos := Pos - 1;
   end loop;

   -------------------------
   --  Test for Set_LEDs  --
   -------------------------
   loop
      Set_LEDs (Value);
      delay until Clock + Milliseconds (250);
      Value := Value + 1;
   end loop;

   --delay until Time_Last;

end Pendulum_2;
