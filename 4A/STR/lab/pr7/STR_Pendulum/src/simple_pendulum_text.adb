pragma Ada_2012;
package body Simple_Pendulum_Text is

   -- Binary pattern of whole text to display
   Pattern : array (1 .. Line_Size * Bars_Per_Char) of Byte := (others => 0);

   --------------------
   -- Display_String --
   --------------------

   procedure Display_String (Text : in String) is
      Index : Integer := 1;

   begin
      for Char in 1 .. Line_Size * Bars_Per_Char loop
        for Column in 0 .. 4 loop
            if Index <= Text'Length * Bars_Per_Char then
                Pattern (Index) := Char_Map (Text (Char), Column);
            end if;
               Index := Index + 1;
        end loop;
        Index := Index + 1;
      end loop;
   end Display_String;

   --------------------
   ----- Displayer ----
   --------------------

   task Displayer with Priority => System.Priority'Last;

   task body Displayer is
      Cycle_Init_Time : Time; -- Start time of next pendulum cycle
      Cycle_Period    : Time_Span; -- Pendulum period
      Inter_Bar_Delay : Time_Span; -- Delay between consecutive bars
      Semicycle_Start : Time;      -- Time to start switching LEDs
      Flashing_Time   : constant Time_Span := Microseconds (200);
      -- LEDs flashing time
      Next            : Time;

   begin
      loop
         Wait_For_Next_Cycle (Cycle_Init_Time, Cycle_Period);
         Inter_Bar_Delay := (Cycle_Period / 2) / ((Line_Size * Bars_Per_Char) +
                                                 (2 * Margin));
         Semicycle_Start := Cycle_Init_Time + (Margin * Inter_Bar_Delay);
         Next := Semicycle_Start;
         delay until Next;
         -- First semicycle (left to right)
         for Char in Pattern'Range loop
            Set_LEDs (Pattern (Char));
            delay until Next + Flashing_Time;
            Clear_All_LEDs;
            Next := Next + Inter_Bar_Delay;
            delay until Next;
         end loop;

         -- 40 = 20 (left to rigth) + 20 (right to left)
         Next := Next + (Inter_Bar_Delay * 40);
         delay until Next;

         for Char in reverse Pattern'Range loop
            Set_LEDs (Pattern (Char));
            delay until Next + Flashing_Time;
            Clear_All_LEDs;
            Next := Next + Inter_Bar_Delay;
            delay until Next;
         end loop;

      end loop;

   end Displayer;


end Simple_Pendulum_Text;
