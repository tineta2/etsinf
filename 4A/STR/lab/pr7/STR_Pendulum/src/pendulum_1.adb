--  Insert context clauses
with Ada.Real_Time; use Ada.Real_Time;
with STM32.GPIO; use STM32.GPIO;
with STM32.Device; use STM32.Device;


procedure Pendulum_1 is

   --  GPIO_Points of Pendulum LEDs (outputs, top to bottom of bar)
   LED_Points : GPIO_Points (1 .. 8) :=
     (PC4, PB0, PA2, PC2, PA1, PC1, PA3, PC5);
   Next : Time;
   Period : Time_Span := Milliseconds (1000);
   procedure Initialise_Pendulum is
   begin
      --  Enable GPIO ports of LED_Points
      Enable_Clock (LED_Points);

      --  Configure GPIOs of LED_Points
      Configure_IO (Points => LED_Points,
                    Config => (Mode        => Mode_Out,
                               Resistors   => Floating,
                               Output_Type => Push_Pull,
                               Speed       => Speed_100MHz));

      --  Configuration of additional pendulum pins
      --  ...
   end Initialise_Pendulum;


begin
   --  Exercise 1
   Initialise_Pendulum;
   Set (LED_Points);
   Next := Clock + Period;
   for Count in 1..8 loop
      Clear (LED_Points(Count));
      delay until Next;
      Next := Next + Period;
   end loop;

   for Count in 1..8 loop
      Set (LED_Points(Count));
      delay until Next;
      Next := Next + Period;
   end loop;
end Pendulum_1;
