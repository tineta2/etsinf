/* Calculo de PI. Cada proceso acumula la suma parcial de un subintervalo */
  h   = 1.0 / (double) n; 
  sum, t1, t2 = 0.0; 
  t1 = MPI_Wtime()*1000;
  #pragma omp parallel for private(x) reduction(+:sum) 
  for (i = myid+1; i <= n; i += numprocs) { 
    x = h * ((double)i - 0.5); 
    sum += (4.0 / (1.0 + x*x)); 
  }
   
  mypi = h * sum; 
  if (myid != 0) {
    MPI_Send(&mypi, 1, MPI_DOUBLE, 0, 100, MPI_COMM_WORLD); 
  }  else { 
/* Reduccion: el proceso 0 obtiene la suma de todos los resultados */
    //pi +=mypi;
    MPI_Sendrecv(&mypi, 1, MPI_DOUBLE, 0, 4, &pi, 1, MPI_DOUBLE, 0, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    for (i = 1; i < numprocs; i++) {
      MPI_Recv(&mypi, numprocs, MPI_DOUBLE, i, 100, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      pi += mypi;
    }
  }

  t2 = (MPI_Wtime()*1000) - t1;
