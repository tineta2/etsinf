/* Calculo de PI. Cada proceso acumula la suma parcial de un subintervalo */
  h   = 1.0 / (double) n; 
  sum, t1, t2 = 0.0; 
  t1 = MPI_Wtime()*1000; 
  #pragma omp parallel for private(x) reduction(+:sum)
  for (i = myid + 1; i <= n; i += numprocs) { 
    x = h * ((double)i - 0.5); 
    sum += (4.0 / (1.0 + x*x)); 
  } 
  mypi = h * sum; 
  /* Reduccion: el proceso 0 obtiene la suma de todos los resultados */
  MPI_Reduce(&mypi, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
  t2 = (MPI_Wtime()*1000) - t1;

