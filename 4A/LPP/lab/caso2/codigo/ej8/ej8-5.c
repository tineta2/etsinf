int mensaje[10}, count = 0, i;
MPI_Status status;

for( i=0; i<nneighbors; i++ ) {
  if( neighbors[i] < rank ) { // si mi vecino es mi padre:
    MPI_Recv(mensaje, 10, MPI_INT, neighbors[i], MPI_ANY_TAG, MPI_COMM_GRAPH, &status); // recibo un mensaje de mi padre

    // Averiguo cuantos elementos tenia el mensaje recibido y lo almaceno en la variable count
    MPI_Get_count(&status, MPI_INT, &count);
  } else { // si mi venico es mi hijo:
    mensaje[count] = rank; // Escribo mi identificador al final del vector mensaje

    // Envio a mi hijo el nuevo mensaje, con su tamanyo actualizado
    MPI_Send(mensaje, count+1, MPI_INT, neighbors[i], 0, MPI_COMM_GRAPH);
  }
}
