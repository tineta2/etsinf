if (myid == 0) { //maestro
   i = 0;
   encontrado = 0;
   esclavos = numprocs - 1;
   while (esclavos > 0) {
      //recibir msg de cualquier esclavo
	  MPI_Recv(&n, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
	  if (stat.MPI_TAG == SOLUCION_ENCONTRADA) {
	  //imprimir num primo enviado por esclavo
	     if (nprimos < max) {
	        printf("Numero primo %d de esclavo %d\n", n, stat.MPI_SOURCE);
	        nprimos++;
	        n = rand();
		       MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, BUSCAR_PRIMO, MPI_COMM_WORLD);
	        }
	     else {
	        MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, FIN, MPI_COMM_WORLD);
		    esclavos--;
	     }
	   else {
	      n = rand();
		  //enviar num (ETIQ=BUSCAR_PRIMO) al esclavo que envio msg anterior
		  MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, BUSCAR_PRIMO, MPI_COMM_WORLD);
	   }
   }
}

