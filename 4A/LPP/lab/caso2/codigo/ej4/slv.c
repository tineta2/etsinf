 else {  //esclavos
   MPI_Send(&n, 1, MPI_INT, 0, ESCLAVO_PREPARADO, MPI_COMM_WORLD);
   continuar = 1;
   while (continuar) {
      //recibir msg con numero = n
      MPI_Recv(&n, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
      if (stat.MPI_TAG == FIN) {
      continuar = 0;
      }
      else {
         //averiguar si n es primo
         int primo;
         primo = isPrime(n);
         if (primo) {
         //enviar n a maestro (ETIQ=SOLUCION_ENCONTRADA)
         MPI_Send(&n, 1, MPI_INT, 0, SOLUCION_ENCONTRADA, MPI_COMM_WORLD);
	     }
         else {
            //enviar msg al maestro (ETIQ=ESCLAVO_PREPARADO)
	        MPI_Send(&n, 1, MPI_INT, 0, ESCLAVO_PREPARADO, MPI_COMM_WORLD);
	     }
	  }
   }
}
