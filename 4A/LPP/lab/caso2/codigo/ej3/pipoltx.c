for (j = 0; j < nreps; j++) {
          if (myid == 0) {
          twn = MPI_Wtime()*1000;
	  MPI_Send(msg, n, MPI_BYTE, 1, 100, MPI_COMM_WORLD);
	  MPI_Recv(msg, n, MPI_BYTE, 1, 100, MPI_COMM_WORLD, &stat);
	  twn = (MPI_Wtime()*1000) - twn;
	  tw += twn;
       }
       if (myid == 1) {
          MPI_Recv(msg, n, MPI_BYTE, 0, 100, MPI_COMM_WORLD, &stat);
	  MPI_Send(msg, n, MPI_BYTE, 0, 100, MPI_COMM_WORLD);
       }
     }
    
    ts = (MPI_Wtime()*1000) - ts;

    if (myid == 0) {
           
       tc = ts + (n*(tw / nreps));

       printf("Tiempo comunicacion (ts): %lf\n", ts);
       printf("Tiempo comunicacion (tw) (media de %d repeticiones): %lf\n", nreps, tw/nreps);
       printf("Tiempo comunicacion (tc): %lf\n", tc);
    }
