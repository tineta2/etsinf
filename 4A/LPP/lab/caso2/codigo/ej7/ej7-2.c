MPI_Comm commrow, commcol;
int belongs[2];

// Submallas de las filas
belongs[0] = 0;
belongs[1] = 1; // la dimension "y" (columnas) pertenece a la submalla con comunicador commrow
MPI_Cart_sub(new_comm, belongs, &commrow);

// Submallas de las columnas
belongs[0] = 1; // la dimension "x" (filas) pertenece a la submalla con comunicador commcol
belongs[1] = 0;
MPI_Cart_sub(new_comm, belongs, &commcol);
