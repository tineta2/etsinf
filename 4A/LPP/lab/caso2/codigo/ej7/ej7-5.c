double Cc[N];

// P00 recibe en Cc la C de P10
// y P01 recibe en Cc la C de P11
MPI_Gather(&C, 1, MPI_DOUBLE, &Cc, 1, MPI_DOUBLE, 0, commcol);

// P00 recibe en C0 el resto de la matriz que tenia P01 en Cc
if (coords[0] == 0) {
    MPI_Gather(Cc, 2, MPI_DOUBLE, &C0, 2, MPI_DOUBLE, 0, commrow);
}
