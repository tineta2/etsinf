double Ca[N], double Cb[N];
double a,b;

// El proceso P00 reparte la matriz A0 y B0 entre los procesos de la primera fila (el mismo y P01)
if (coords[0] == 0) {
  // P00 reparte el vector A0 de la siguiente forma:
  // P00: Ca = {6.0, 0.2} y P01: Ca = {5.8, 8.0}
  MPI_Scatter(A0, 2, MPI_DOUBLE, Ca, 2, MPI_DOUBLE, 0, commrow);

  // Paso 1: El P00 envia las columnas a su fila de procesos
  // P00 reparte el vector B0 de la siguiente forma:
  // P00: Cb: {0.2, 3.7} y P01: Cb = {9.4, 6.8}
  MPI_Scatter(B0, 2, MPI_DOUBLE, Cb, 2, MPI_DOUBLE, 0, commrow);
}

// Los procesos P00 y P01 reparten sus vectores Ca y Cb con los procesos P10 y P11, respectivamente:
// P00: a = 6.0 y P10: a = 0.2
// P01: a = 5.8 y P11: a = 8.0
MPI_Scatter(Ca, 1, MPI_DOUBLE, &a, 1, MPI_DOUBLE, 0, commcol);

// Los procesos P000 y P01 reparten sus vectoress Ca y Cb con los procesos P10 y P11, respectivamente:
// P00: b = 0.2 y P10: b = 3.7
// P01: b = 9.4 y P11: b = 6.8
MPI_Scatter(Cb, 1, MPI_DOUBLE, &b, 1, MPI_DOUBLE, 0, commcol);
