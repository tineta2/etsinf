MPI_Comm new_comm;
int ndims, reorder, periods[2], dim_size[2];

ndims = 2; // malla 2D
dim_size[0] = 2; // filas
dim_size[1] = 2; //columnas
periods[0] = 0; // filas no-periodicas
periods[1] = 0; // columnas no-periodicas
reorder = 0;

MPI_Cart_create(MPI_COMM_WORLD, ndims, dim_size, periods, reorder, &new_comm);

// Cada proceso se guarda sus propias coordenadas en el vector coords --> {00, 01, 10, 11}
int coords[4];
MPI_Cart_coords(new_comm, rank, 4, coords);
