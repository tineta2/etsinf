void maestro( int n, double *x, int t, double *I, double *R, int lda, double *b, double nrm, int p ) {
   int k, inc = 1;
   if( n==0 ) {
     if( nrm < minimo ) {
       minimo = nrm;
       dcopy_( &lda, x, &inc, sol, &inc );
     }
   } else {
     for( k=0; k<t; k++ ) {
       int m = n-1;
       x(m) = I(k);
       double r = R(m,m)*x(m) - b(m);
       double norma = nrm + r*r;
       if( norma < minimo ) {
         double v[m];
         double y[lda];
         dcopy_( &m, b, &inc, v, &inc );
         dcopy_( &lda, x, &inc, y, &inc );
         double alpha = -x(m);
         daxpy_( &m, &alpha, &R(0,m), &inc, v, &inc );
         if (p < depth) { // Si el maestro aun no ha llegado a la profundiad depth, sigue trabajando
    	      maestro(m, y, t, I, R, lda, v, norma, p+1);
         } else { // Si el maestro ya ha llegado a la profundidad depth, manda a los esclavos a trabajar
           // Recibo datos de un esclavo preparado para trabajar
    	     MPI_Recv(&aux, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat); // Hay algun esclavo libre?
           if (stat.MPI_TAG == FIN) { // Si es un esclavo que estaba trabajando y ya ha terminado:
             // Recibo el resto de la solucion
             MPI_Recv(solTemp, n, MPI_DOUBLE, stat.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
             // Compruebo la solucion
   	         if (aux < minimo) {
               minimo = aux;
               memcpy(sol, solTemp, n*sizeof(double));
             }
	         }
           // envio datos al esclavo para que haga trabajo (m,y,v,norma)
           // las variables t,I,R,lda no hace falta enviarlas porque nunca se modifican
           // enviar un mensaje por cadad dato, ver si enviar min y sol, simplemente por darles una referencia, lo pueden hacer localmente.
	         MPI_Send(&m, 1, MPI_INT, stat.MPI_SOURCE, 100, MPI_COMM_WORLD);
    	     MPI_Send(y, m, MPI_DOUBLE, stat.MPI_SOURCE, 200, MPI_COMM_WORLD);
    	     MPI_Send(v, m, MPI_DOUBLE, stat.MPI_SOURCE, 700, MPI_COMM_WORLD);
    	     MPI_Send(&norma, 1, MPI_DOUBLE, stat.MPI_SOURCE, 800, MPI_COMM_WORLD);
	       }
       }
     }
    }
 }
