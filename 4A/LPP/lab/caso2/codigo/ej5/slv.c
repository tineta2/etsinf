else { // Esclavos:
   // Enviar mensaje inicial al Maestro con etiqueta=ESCLAVO_PREPARADO
   MPI_Send(&aux, 1, MPI_DOUBLE, 0, ESCLAVO_PREPARADO, MPI_COMM_WORLD);
   continuar = 1;
   while (continuar) { // mientras continuar != 0
      // Recibir mensaje del Maestro con numero n
      MPI_Recv(&n, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
      // Comprobacion de la etiqueta del mensaje
      if (stat.MPI_TAG == FIN) { // Si el esclavo recibe un mensaje de terminacion, termina
         continuar = 0;
      }
      else {
         MPI_Recv(x, n, MPI_DOUBLE, 0, 200, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         MPI_Recv(b, n, MPI_DOUBLE, 0, 700, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         MPI_Recv(&nrm, 1, MPI_DOUBLE, 0, 800, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         esclavo(n, x, t, I, R, lda, b, nrm);
         // Una vez el esclavo termina de ejecutar el algoritmo, envia las minimo y sol al maestro
         MPI_Send(&minimo, 1, MPI_DOUBLE, 0, FIN, MPI_COMM_WORLD);
         MPI_Send(sol, n, MPI_DOUBLE, 0, FIN, MPI_COMM_WORLD);
	 }
  }
}

