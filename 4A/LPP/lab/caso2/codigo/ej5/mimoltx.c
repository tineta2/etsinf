#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <mpi.h>
#include <math.h>

#define	R(i,j)	R[j*lda+i]
#define	b(i)	b[i]
#define	x(i)	x[i]
#define	I(i)	I[i]
#define	sol(i)	sol[i]
#define NMAX    4
#define ENCONTRADO 1
#define BUSCAR_ESCLAVO 10
#define ESCLAVO_PREPARADO 20
#define FIN 50

void printMatrix( const char *s, double *R, int lda, int n );
void maestro( int n, double *x, int t, double *I, double *R, int lda, double *b, double nrm, int p );
void esclavo( int n, double *x, int t, double *I, double *R, int lda, double *b, double nrm );

double minimo, minTemp, aux;
double *sol, *solTemp;

#ifdef CONTAR_TAREAS
int tareas=0;
#endif

// Variables MPI
MPI_Status stat;
int myid, nprocs, tag, depth, p, continuar, esclavos, esclavosActivos;
double tiempo;
char msg[NMAX];

int main( int argc, char *argv[] ) {
  int n, N, i, j, t;
  double *R, *I, *b, *x;

  // Inicialización MPI
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);


  if( argc<4 ) {
    fprintf(stderr,"usage: %s n(tamano de la matriz) t(tamano del conjunto discreto) d(profundidad del mastro) \n",argv[0]);
    exit(-1);
  }
  sscanf(argv[1],"%d",&n);
  /* Reservamos espacio para una matriz cuadrada */
  int lda = n;
  N = n*n;
  if( ( R = (double*) malloc(N*sizeof(double)) ) == NULL ) {
    fprintf(stderr,"Error en la reserva de memoria para la matriz A\n");
    exit(-1);
  }
  srand(1234);
  /* Generamos aleatoriamente la parte triangular superior */
  for( i=0; i<n; i++ ) {
    for( j=i; j<n; j++ ) {
      R(i,j) = ((double) rand()) / RAND_MAX;
    }
  }

#ifdef DEBUG
  /* Imprimir matriz */
  printMatrix( "R = ", R, lda, n );
#endif

  /* Reservamos espacio para el vector independiente */
  if( ( b = (double*) malloc(n*sizeof(double)) ) == NULL ) {
    fprintf(stderr,"Error en la reserva de memoria para el vector b\n");
    exit(-1);
  }
  /* Generamos aleatoriamente el vector independiente */
  for( i=0; i<n; i++ ) {
    b(i) = ((double) rand()) / RAND_MAX;
  }

#ifdef DEBUG
  /* Imprimir vector */
  printf("b = [");
  for( i=0; i<n; i++ ) {
    printf("%10.4lf",b(i));
  }
  printf("    ] \n");
#endif

  /* Leemos por teclado el cardinal del conjunto discreto de elementos */
  sscanf(argv[2],"%d",&t);
  if( t%2 ) {
    fprintf(stderr,"El valor de t ha de ser par \n");
    exit(-1);
  }
  /*Leemos por teclado la profundidad maxima del maestro*/
  sscanf(argv[3], "%d", &depth);

  /* Reservamos espacio para el vector de elementos del conjunto discreto */
  if( ( I = (double*) malloc(t*sizeof(double)) ) == NULL ) {
    fprintf(stderr,"Error en la reserva de memoria para el conjunto A\n");
    exit(-1);
  }
  for( i=0; i<t; i++ ) {
    I(i) = (2*i+1-t)/2.0;
  }
#ifdef DEBUG
  /* Imprimir conjunto */
  printf("I = (");
  for( i=0; i<t; i++ ) {
    printf("%10.2lf",I(i));
  }
  printf("    )\n");
#endif

  /* Reservamos espacio para el vector solucion */
  if( ( x = (double*) malloc(n*sizeof(double)) ) == NULL ) {
    fprintf(stderr,"Error en la reserva de memoria para el vector x\n");
    exit(-1);
  }

  /* Llamada a la funcion que minimiza el problema de minimos cuadrados */
  minimo = 1.0/0.0; //(double) RAND_MAX;
  sol = (double*) malloc(n*sizeof(double));
  solTemp = (double*) malloc(n*sizeof(double));
  double nrm = 0.0;
  // Código Maestro-Esclavo
  if (myid == 0) { // si soy el nodo raíz soy el maestro
     esclavos = nprocs-1;
      
     
     // El número de esclavos realizando tareas actualmente
     // El maestro ejecuta el algoritmo1 con profundida=0
   // while() { 
     maestro( n, x, t, I, R, lda, b, nrm, 0 );
     
     // Si el maestro termina antes de que todos los esclavos terminen,
     // entonces me quedo esperando para recibir sus datos
     
     
     for (i=0; i<esclavos; i++) {
	
	     MPI_Recv(&minTemp, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
	    if (stat.MPI_TAG == FIN) {
         MPI_Recv(solTemp, n, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	        if (minTemp < minimo) {
            minimo = minTemp;
            memcpy(sol, solTemp, n*sizeof(double));
            }
	      }
       MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, FIN, MPI_COMM_WORLD); //enviar etiqueta terminación
     }
   //}   
  }
  else { // Esclavos:

     // Enviar mensaje inicial al Maestro con etiqueta=ESCLAVO_PREPARADO
     MPI_Send(&aux, 1, MPI_DOUBLE, 0, ESCLAVO_PREPARADO, MPI_COMM_WORLD);
     continuar = 1;

     while (continuar) { // mientras continuar != 0
        
       // Recibir mensaje del Maestro con número n
       MPI_Recv(&n, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
       

       // Comprobación de la etiqueta del mensaje
       if (stat.MPI_TAG == FIN) { // Si el esclavo recibe un mensaje de terminación, termina
         continuar = 0;
       }
       else {
         MPI_Recv(x, n, MPI_DOUBLE, 0, 200, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         MPI_Recv(b, n, MPI_DOUBLE, 0, 700, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         MPI_Recv(&nrm, 1, MPI_DOUBLE, 0, 800, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         
         esclavo(n, x, t, I, R, lda, b, nrm);
         
         // Una vez el esclavo termina de ejecutar el algoritmo, envía las minimo y sol al maestro

         MPI_Send(&minimo, 1, MPI_DOUBLE, 0, FIN, MPI_COMM_WORLD);
         MPI_Send(sol, n, MPI_DOUBLE, 0, FIN, MPI_COMM_WORLD);
	 
	}
    }
  }

  //double t2 = omp_get_wtime();


  if (myid == 0) { // si soy el nodo raíz imprimo la solución
   //printf("Tiempo = %lf\n",t2-t1);
    //printf("Tiempo = %lf\n", time);
    printf("Minimo = %lf\n",minimo);
    printf("sqrt(minimo) = %lf\n",sqrt(minimo));
    printf("x = (");
    for( i=0; i<n; i++ ) {
      printf("%10.2lf",sol(i));
    }
    printf("    )\n");
  }
  #ifdef CONTAR_TAREAS
    printf("tareas = %d\n",tareas);
  #endif

  free(x);
  free(I);
  free(R);
  free(b);
  free(sol);
  //free(solTemp);

  MPI_Finalize();
}

void printMatrix( const char *s, double *R, int lda, int n ) {
  int i, j;
  for( i=0; i<n; i++ ) {
    for( j=0; j<i; j++ ) {
      printf("%10.4lf",0.0);
    }
    for( j=i; j<n; j++ ) {
      printf("%10.4lf",R(i,j));
    }
    printf("\n");
  }
}

/**
    Algoritmo1 del enunciado de la práctica en el que se trabaj con el cuadrado de la norma y se va calculando según    .
    se sugiere en la misma memoria.

    n	(int)		Tamaño del subvector de x sobre el que se está trabajando, x(0:n-1). Las componentes x(n:lda) se han calculado ya.
    x	(double*)	Puntero al primer elemento del vector x (completo).
    t	(int)		Tamaño del conjunto discreto I de símbolos.
    I	(double*)	Puntero al primer elemento del vector donde se encuentran los elementos del conjunto discreto I.
    R	(double*)	Puntero al primer elemento de la matriz triangular superior R de tamaño lda x n.
    lda	(int)		(Leading dimension) Número de filas de la matriz R. Coincide con el tamaño de los vectores x y b en la primera llamada a la función.
    b	(double*)	Puntero al primer elemento del vector b (completo).
    nrm	(double)	Norma calculada hasta el momento de la llamada a este algoritmo.

    El vector x es de entrada/salida, los demás argumentos son solo de entrada.
*/
void maestro( int n, double *x, int t, double *I, double *R, int lda, double *b, double nrm, int p ) {
  int k, inc = 1;
  if( n==0 ) {
    if( nrm < minimo ) {
      minimo = nrm;
      dcopy_( &lda, x, &inc, sol, &inc );
    }
  } else {
      for( k=0; k<t; k++ ) {
        int m = n-1;
        x(m) = I(k);
        double r = R(m,m)*x(m) - b(m);
        double norma = nrm + r*r;
        if( norma < minimo ) {
          double v[m];
          double y[lda];
          dcopy_( &m, b, &inc, v, &inc );
          dcopy_( &lda, x, &inc, y, &inc );
          double alpha = -x(m);
          daxpy_( &m, &alpha, &R(0,m), &inc, v, &inc );

    	  if (p < depth) { // Si el maestro aún no ha llegado a la profundiad depth, sigue trabajando
    	      maestro(m, y, t, I, R, lda, v, norma, p+1);

          } else { // Si el maestro ya ha llegado a la profundidad depth, manda a los esclavos a trabajar

          // Recibo datos de un esclavo preparado para trabajar
    	  MPI_Recv(&aux, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat); // Hay algún esclavo libre?

          if (stat.MPI_TAG == FIN) { // Si es un esclavo que estaba trabajando y ya ha terminado:

            // Descuento el número de esclavos activos
            // Recibo el resto de la solución
	    //MPI_Recv(&minTemp, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 1000, MPI_COMM_WORLD, &stat);
            

	    MPI_Recv(solTemp, n, MPI_DOUBLE, stat.MPI_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            // Compruebo la solución
   	    if (aux < minimo) {
              minimo = aux;
	      //dcopy_(&lda, solTemp, &inc, sol, &inc);
	      //
              
	      memcpy(sol, solTemp, n*sizeof(double));
            }
	    
	   }
          // envío datos al esclavo para que haga trabajo (m,y,v,norma)
          // las variables t,I,R,lda no hace falta enviarlas porque nunca se modifican
          // enviar un mensaje por cadad dato, ver si enviar min y sol, simplemente por darles una referencia, lo pueden hacer localmente.
	    MPI_Send(&m, 1, MPI_INT, stat.MPI_SOURCE, 100, MPI_COMM_WORLD);
    	    MPI_Send(y, m, MPI_DOUBLE, stat.MPI_SOURCE, 200, MPI_COMM_WORLD);
    	    MPI_Send(v, m, MPI_DOUBLE, stat.MPI_SOURCE, 700, MPI_COMM_WORLD);
    	    MPI_Send(&norma, 1, MPI_DOUBLE, stat.MPI_SOURCE, 800, MPI_COMM_WORLD);
	    
	  }
       }
     }
    }
  }


void esclavo( int n, double *x, int t, double *I, double *R, int lda, double *b, double nrm ) {
  int k, inc = 1;

  if (n == 0) {
     if (nrm < minimo) {
        //#pragma omp critical
        //if( nrm < minimo ) {
           minimo = nrm;
           dcopy_( &lda, x, &inc, sol, &inc );
	   
        //}
     }
   } else {
        for( k=0; k<t; k++ ) {
            int m = n-1;
            x(m) = I(k);
            double r = R(m,m)*x(m) - b(m);
            double norma = nrm + r*r;
            if( norma < minimo ) {
              double v[m];
              double y[lda];
              dcopy_( &m, b, &inc, v, &inc );
              dcopy_( &lda, x, &inc, y, &inc );
              double alpha = -x(m);
              daxpy_( &m, &alpha, &R(0,m), &inc, v, &inc );

              //#pragma omp task
              esclavo( m, y, t, I, R, lda, v, norma );
            }
         }
      }
    
}
