// Codigo Maestro-Esclavo
if (myid == 0) { // si soy el nodo raiz soy el maestro
   esclavos = nprocs-1;
   maestro( n, x, t, I, R, lda, b, nrm, 0 );
   // Si el maestro termina antes de que todos los esclavos terminen,
   // entonces me quedo esperando para recibir sus datos
   for (i=0; i<esclavos; i++) {
      MPI_Recv(&minTemp, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
	  if (stat.MPI_TAG == FIN) {
         MPI_Recv(solTemp, n, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
         if (minTemp < minimo) {
         minimo = minTemp;
         memcpy(sol, solTemp, n*sizeof(double));
         }
       }
       MPI_Send(&n, 1, MPI_INT, stat.MPI_SOURCE, FIN, MPI_COMM_WORLD); //enviar etiqueta terminacion
   }   
}
