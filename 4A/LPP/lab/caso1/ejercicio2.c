
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

int main( int argc, char *argv[] ) {

  long int n;
  if( argc<2 ) {
    fprintf(stderr,"Uso: %s n\n",argv[0]);
    return -1;
  }

  sscanf(argv[1],"%ld",&n);

  /* Generación aleatoria del vector */
  double *v = (double*) malloc(n*sizeof(double));
  long int i, j;
  for( i=0; i<n; i++ ) {
    v[i] = ((double) rand()) / RAND_MAX;
  }

  /**************************************************************/
  /* Código que realiza los cálculos requeridos en el enunciado */
  /**************************************************************/
  double media, vari, desv, max, min, time;
  media, vari, desv = 0.0;
  max, min = v[0];

  time = omp_get_wtime();

  #pragma omp parallel
  {
	#pragma omp for
  	for (j = 0; j < n; j++) {
		media += v[j];
		if (v[j] > max) {
			#pragma omp critical
			if (v[j] > max)	
				max = v[j];	
		}
		if (v[j] < min) {
			#pragma omp critical
			if (v[j] < min)	
				min = v[j];	
		}
  	}
  	media /= n;
  	#pragma omp for nowait
  	for (j = 0; j < n; j++) {
		vari += pow((v[j] - media), 2);
  	}
  	vari /= n;
  
  	#pragma omp for nowait
  	for (j = 0; j < n; j++) {
		desv += pow((v[j] - media), 2);
  	}
  	desv = sqrt(desv / n);
  }

  time = omp_get_wtime();

  printf("Media: %lf\n", media);
  printf("Varianza: %lf\n", vari);
  printf("Desviacion tipica: %lf\n", desv);
  printf("Maximo : %lf\n", max);
  printf("Minimo: %lf\n", min);
  printf("Tiempo: %lf", time);
  /**************************************************************/
  /* Fin del código                                             */
  /**************************************************************/

  free(v);
  return 1;

}

