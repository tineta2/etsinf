import sys

numbers = sys.stdin.readlines()

#Get rid off \n
for num in numbers:
    numbers[numbers.index(num)] = num.replace('\n', '')

d = dict.fromkeys(numbers, "")

#Feed dictionary
for num in numbers:
    d[num] += "*"

#print dictionary
for k, v in d.items():
    print(k + "\t" + v)
