import sys, getopt

def rotor(n, c):
    num = ord(c)
    new_num = num;
    if num >= 65 and num <= 90: #if uppercase alphabet
        new_num = (((num - 64) + int(n)) % 26) + 64
    elif num >= 97 and num <= 122: #if lowercase alphabet
        new_num = (((num - 96) + int(n)) % 26) + 96
    c = chr(new_num)
    return c

try:
    opts, args = getopt.getopt(sys.argv[1:], "n:")
    op, n = opts[0]
    file = open(sys.argv[3], 'r+')
    lines = file.readlines()
    s = ''
    for l in lines:
        for w in l:
            w = rotor(n, w)
            s += w
    file.close()
    file = open(sys.argv[3], 'w+')
    file.write(s)
    file.close()
except getopt.GetoptError as err:
    print(err)
    sys.exit(2)
