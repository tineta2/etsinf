esp_to_eng = {
    "Accesibilidad": "Accessibility",
    "alumno": "student",
    "Becas": "Scholarships",
    "Buscar": "Search",
    "Contacto": "Contact",
    "ESTUDIOS": "STUDIES",
    "grado": "bachelor",
    "Mapa": "Map",
    "PERFILES": "PROFILES",
    "universidad": "university"
}

def proxy():
    import string, cgi, time, re
    from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
    from urllib2 import URLError, HTTPError, urlopen
    class my_server(BaseHTTPRequestHandler):
        def do_GET(self):
            address="http://www.upv.es"+self.path
            url=urlopen(address)
            data=url.read()
            tipo=url.info().getheader("content-type")
            if tipo.find("html") != -1:
                data=data.replace("UPV", "[[[UPV]]]")
                data=re.sub('https?://.*\.\w+', 'http://www.upm.es', data)
                for k in esp_to_eng:
                    data = data.replace(k, esp_to_eng[k])
            self.send_response(200)
            self.send_header("content-type", tipo)
            self.end_headers()
            self.wfile.write(data)
            return 0
        def log_message(self, format, *args):
            print "Client url: ", self.client_address
            return
    server=HTTPServer(("localhost",8081),my_server)
    server.serve_forever()
proxy()
